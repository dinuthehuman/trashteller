import time
from trashteller.gpio import GPIOManager

gpio = GPIOManager()

while True:

	gpio.set_trash(True)
	time.sleep(1)
	gpio.set_trash(False)
	time.sleep(1)
	gpio.set_paper(True)
	time.sleep(1)
	gpio.set_paper(False)
	time.sleep(1)
	gpio.set_paper(True)
	time.sleep(1)
	gpio.set_paper(False)
	time.sleep(1)
	gpio.set_wcard(True)
	time.sleep(1)
	gpio.set_wcard(False)
	time.sleep(1)
	gpio.set_wcard(True)
	time.sleep(1)
	gpio.set_wcard(False)
	time.sleep(1)
	gpio.set_wcard(True)
	time.sleep(1)
	gpio.set_wcard(False)
	time.sleep(1)