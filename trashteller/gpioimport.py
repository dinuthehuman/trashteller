def conditional_import():
	try:
		import RPi.GPIO as GPIO
	except ImportError:
		import GPIOEmu as GPIO
	GPIO.setmode(GPIO.BCM)
	return GPIO
