import datetime
import yaml
import pytz
from pathlib import Path
from importlib import resources


class PaperModule:

    START_TIME = datetime.time(20, 0, 0)
    STOP_TIME = datetime.time(11, 0, 0)

    @staticmethod
    def load_yaml_data() -> list:
        textcontent = resources.read_text("trashteller", "trashdates.yaml")
        yamldata = yaml.safe_load(textcontent)["paper"]
        return yamldata

    @staticmethod
    def set_time_boundaries(occ_date: datetime.date) -> (datetime.datetime, datetime.datetime):
        startdate = occ_date - datetime.timedelta(days=1)
        startdt = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(startdate, PaperModule.START_TIME))
        stopdt = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(occ_date, PaperModule.STOP_TIME))
        return startdt, stopdt

    @staticmethod
    def boundaries_list(datelist: list) -> list:
        return [PaperModule.set_time_boundaries(x) for x in datelist]

    @staticmethod
    def check_current(boundaries: tuple) -> bool:
        now = pytz.timezone('Europe/Zurich').localize(datetime.datetime.now())
        return (boundaries[0] <= now) and (now <= boundaries[1])

    @staticmethod
    def check_paper() -> bool:
        datelist = PaperModule.load_yaml_data()
        boundaries_list = PaperModule.boundaries_list(datelist)
        for i in boundaries_list:
            if PaperModule.check_current(i):
                return True
        return False


if __name__ == "__main__":

    pm = PaperModule()
    print(pm.check_paper())
