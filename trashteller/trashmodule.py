import datetime
import pytz


class TrashModule:

    PICKUP_WD = [0, 3]
    START_TIME = datetime.time(20, 0, 0)
    STOP_TIME = datetime.time(11, 0, 0)

    @staticmethod
    def check_trash() -> bool:
        now = pytz.timezone('Europe/Zurich').localize(datetime.datetime.now())
        today = datetime.date(now.year, now.month, now.day)
        today_evening = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(today, TrashModule.START_TIME))
        today_morning = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(today, TrashModule.STOP_TIME))
        evening_wd = [(x-1) % 7 for x in TrashModule.PICKUP_WD]
        evening_ok = (now.weekday() in evening_wd) and (now >= today_evening)
        morning_ok = (now.weekday() in TrashModule.PICKUP_WD) and (now <= today_morning)
        return evening_ok | morning_ok
