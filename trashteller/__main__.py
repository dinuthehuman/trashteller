from trashteller.gpio import GPIOManager
from trashteller.papermodule import PaperModule
from trashteller.trashmodule import TrashModule
from trashteller.wcardmodule import WCardModule


print("Check module started")

gp = GPIOManager()

paper = PaperModule.check_paper()
gp.set_paper(paper)
print("Set paper led to:", paper)

trash = TrashModule.check_trash()
gp.set_trash(trash)
print("Set trash led to:", trash)

wcard = WCardModule.check_wcard()
gp.set_wcard(wcard)
print("Set wcard led to:", wcard)

print("All checks done, exiting...")
exit(0)
