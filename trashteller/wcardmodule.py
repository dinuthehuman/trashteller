import datetime
import calendar
import pytz


class WCardModule:

    WCARD_WD = 3
    START_TIME = datetime.time(16, 0, 0)
    STOP_TIME = datetime.time(7, 0, 0)

    @staticmethod
    def get_last_friday() -> datetime.date:
        today = datetime.date.today()
        this_month = calendar.monthcalendar(today.year, today.month)
        if this_month[-1][WCardModule.WCARD_WD] == 0:
            fdom = this_month[-2][WCardModule.WCARD_WD]
        else:
            fdom = this_month[-1][WCardModule.WCARD_WD]
        return datetime.date(today.year, today.month, fdom)

    @staticmethod
    def set_time_boundaries(occ_date: datetime.date) -> (datetime.datetime, datetime.datetime):
        startdate = occ_date - datetime.timedelta(days=1)
        startdt = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(startdate, WCardModule.START_TIME))
        stopdt = pytz.timezone('Europe/Zurich').localize(datetime.datetime.combine(occ_date, WCardModule.STOP_TIME))
        return startdt, stopdt

    @staticmethod
    def check_wcard() -> bool:
        boundaries = WCardModule.set_time_boundaries(WCardModule.get_last_friday())
        now = pytz.timezone('Europe/Zurich').localize(datetime.datetime.now())
        return (boundaries[0] <= now) and (now <= boundaries[1])


if __name__ == "__main__":
    print(WCardModule.check_wcard())
