from trashteller.gpioimport import conditional_import


class GPIOManager:

	def __init__(self):
		self.gpio = conditional_import()
		self.pinout = [18, 23, 24, 25]
		self.gpio.setmode(self.gpio.BCM)
		self.setup()

	def setup(self):
		for i in self.pinout:
			self.gpio.setup(i, self.gpio.OUT, initial=0)

	def set_paper(self, val: bool):
		self.gpio.output(self.pinout[2], val)

	def set_trash(self, val: bool):
		self.gpio.output(self.pinout[1], val)

	def set_wcard(self, val: bool):
		self.gpio.output(self.pinout[0], val)

	def set_aux(self, val: bool):
		self.gpio.output(self.pinout[3], val)