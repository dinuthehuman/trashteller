from distutils.core import setup
from setuptools import find_packages

setup(
	name='trashteller',
	author='Martin Obrist',
	author_email='martin.obrist@buero.io',
	version='0.1',
	description='Tells us when to throw stuff away',
	packages=find_packages(),
	include_package_data=True
)
